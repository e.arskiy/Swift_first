import Foundation

// Задание 1

var milkmanPhrase = "Молоко - это полезно"
print (milkmanPhrase)

// Задание 2

var milkPrice: Double = 3 // изменено на double с учетом 3 задания

// Задание 3

milkPrice = 4.20 // изменил во 2 задании с let на var и поменял тип на double чтобы был верный подсчет без округлений

// Задание 4 (уже со звездочкой)

var milkBottleCount: Int? = 20
if let bottles = milkBottleCount {
    let newCount = String(bottles).reversed()
    print ("вроде как развенул бутылки")
} else {
    print("не знаем сколько бутылок")
}
// Как мне кажется нельзя использовать force unwrap потому что по заданию мы не знаем какое значение бутылок по итогу содержит переменная а безопасный разворот проверяет содержит ли переменная значение.
var profit: Double = 0.0
profit = milkPrice * Double(milkBottleCount!)
print (profit)

// Задание 5

var employeesList: [String] = []
employeesList.append("Иван")
employeesList.append("Марфа")
employeesList.append("Андрей")
employeesList += ["Петр", "Геннадий"]
print(employeesList)

// Задание 6

var isEveryoneWorkHard: Bool = false
var workingHours: Int = 40
if workingHours >= 40 {
    isEveryoneWorkHard = true
}
print(isEveryoneWorkHard)
workingHours = 35
if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)

